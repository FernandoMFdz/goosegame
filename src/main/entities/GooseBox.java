package main.entities;

public class GooseBox {

    private int positionX;
    private int positionY;
    private int type;

    public GooseBox(int positionX, int positionY,int type) {
        this.setPositionX(positionX);
        this.setPositionY(positionY);
        this.setType(type);
    }


    public int getPositionX() {
        return positionX;
    }

    private void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    private void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public int getType() {
        return type;
    }

    private void setType(int type) {
        this.type = type;
    }

    public static class GooseBoxType {
        public final static int INICIO = -2;
        public final static int OCA = -1;
        public final static int NORMAL = 0;
        public final static int PUENTE = 1;
        public final static int POSADA = 2;
        public final static int POZO = 3;
        public final static int LABERINTO = 4;
        public final static int DADOS = 5;
        public final static int CARCEL = 6;
        public final static int CALAVERA = 7;
        public final static int FINAL = 8;
    }

    public String getDescription(){
        String description = "";
        switch (this.getType()){
            case GooseBoxType.INICIO:
                description = "Justo el inicio, buena suerte!";
                break;
            case GooseBoxType.OCA:
                description = "De Oca a Oca y tiro porque me toca!";
                break;
            case GooseBoxType.PUENTE:
                description = "¡Has caído en el Puente! Saltas a la casilla 19 y pierdes el siguiente turno!";
                break;
            case GooseBoxType.POSADA:
                description = "¡Posada! Pierdes un turno :(";
                break;
            case GooseBoxType.POZO:
                description = "¡Vaya! Estás en el Pozo, y no el de Futbol Sala!";
                break;
            case GooseBoxType.LABERINTO:
                description = "Has caído en el laberinto. Retrocedes hasta la casilla 30";
                break;
            case GooseBoxType.DADOS:
                description = "Se suma de nuevo la marcación del dado. ¡La cosa avanza!";
                break;
            case GooseBoxType.CARCEL:
                description = "Carcel. Te quedas 2 turnos sin jugar :/";
                break;
            case GooseBoxType.CALAVERA:
                description = "¡Calavera!. Vuelves a la casilla 1. ¿Game Over? ";
                break;
            case GooseBoxType.FINAL:
                description = "Fin del Juego. ¡Has ganado!";
                break;
            default:
                description = "Con lo que has sacado en el dado avanzas!";
                break;
        }

        return description;


    }


}
