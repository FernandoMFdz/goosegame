package main.entities;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.text.Text;
import javafx.util.Duration;
import javafx.scene.image.ImageView;

import java.util.Map;

public class Player extends Board {
    private ImageView ficha;
    private int idPlayer;

    private int currentPosition;
    private int turnsBanned = 0;
    public int turnsInGooseBox = 0;

    public Text playerPosition;
    public Text scoreBoard;

    public GooseBox currentGooseBox;


    public Player(ImageView ficha,Text scoreBoard,Text playerPosition) {
        this.setFicha(ficha);
        this.scoreBoard = scoreBoard;
        this.playerPosition = playerPosition;
    }

    public Player() {}

    public void move(int position, GooseBox gooseBox, int resultadoDado, Text boxDescription){
        if(gooseBox != null){
            boxDescription.setText(gooseBox.getDescription());
            this.setCurrentGooseBox(gooseBox);
            this.setCurrentPosition(position);
            this.setBoxEffect(gooseBox,resultadoDado);
            Timeline timeline = new Timeline();
            timeline.getKeyFrames().add(new KeyFrame( Duration.seconds(1),new KeyValue(this.getFicha().layoutXProperty(), this.getCurrentGooseBox().getPositionX())));
            timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(1),new KeyValue(this.getFicha().layoutYProperty(), this.getCurrentGooseBox().getPositionY())));
            timeline.play();
        } else {
            boxDescription.setText("Puntuación demasiado alta. Prueba en el siguiente turno!");
        }
    }

    public ImageView getFicha() {
        return ficha;
    }

    public void setFicha(ImageView ficha) {
        this.ficha = ficha;
    }

    public void setIdPlayer(int idPlayer) {
        this.idPlayer = idPlayer;
    }

    public int getIdPlayer() {
        return idPlayer;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
        this.playerPosition.setText(String.valueOf(currentPosition));
    }

    public boolean isBanned() {
        int turns = this.getTurnsBanned();
        if(turns != 0){
            return true;
        } else {
            return false;
        }
    }

    public int getTurnsBanned() {
        return this.turnsBanned;
    }

    public void setTurnsBanned(int turnsBanned) {
        this.turnsBanned = turnsBanned;
    }

    public void setBoxEffect(GooseBox box, int resultadoDado){
        switch (box.getType()){
            case GooseBox.GooseBoxType.INICIO:
                break;
            case GooseBox.GooseBoxType.OCA:
                if(this.turnsInGooseBox == 1){
                    this.turnsInGooseBox = 0;
                } else {
                    this.turnsInGooseBox = 3;
                }
                for (Map.Entry<Integer, GooseBox> entry : this.getBoxes().entrySet()) {
                    Integer position = entry.getKey();
                    GooseBox gooseBox = entry.getValue();
                    if (position > this.currentPosition) {
                        if (gooseBox.getType() == GooseBox.GooseBoxType.OCA) {
                                if(turnsInGooseBox > 0){
                                    this.setCurrentGooseBox(super.getBoxes().get(position));
                                    this.setCurrentPosition(position);
                                    this.turnsInGooseBox--;
                                }
                            break;
                        }
                    }
                }
                break;
            case GooseBox.GooseBoxType.PUENTE:
                this.setCurrentGooseBox(super.getBoxes().get(19));
                this.setCurrentPosition(19);
                this.setTurnsBanned(1);
                break;
            case GooseBox.GooseBoxType.POSADA:
                this.setTurnsBanned(1);
                break;
            case GooseBox.GooseBoxType.POZO:
                this.setTurnsBanned(-1);
                break;
            case GooseBox.GooseBoxType.LABERINTO:
                this.setCurrentGooseBox(super.getBoxes().get(30));
                this.setCurrentPosition(30);
                break;
            case GooseBox.GooseBoxType.DADOS:
                this.setCurrentGooseBox(super.getBoxes().get(this.getCurrentPosition()+resultadoDado));
                this.setCurrentPosition(this.getCurrentPosition()+resultadoDado);
                break;
            case GooseBox.GooseBoxType.CARCEL:
                this.setTurnsBanned(2);
                break;
            case GooseBox.GooseBoxType.CALAVERA:
                this.setCurrentGooseBox(super.getBoxes().get(1));
                this.setCurrentPosition(1);
                break;
            case GooseBox.GooseBoxType.FINAL:
                break;
            default:
                break;
        }


    }

    public GooseBox getCurrentGooseBox() {
        return currentGooseBox;
    }

    public void setCurrentGooseBox(GooseBox currentGooseBox) {
        this.currentGooseBox = currentGooseBox;
    }



}
