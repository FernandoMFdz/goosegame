package main.entities;

import javafx.scene.text.Text;
import main.controllers.GameScreenController;
import main.entities.GooseBox.GooseBoxType;

import java.util.*;

public class Board  extends GameScreenController {
    private List<Player> playerList = new ArrayList<>();

    // LinkedHashMap para mantener el orden de los indices y que la lista este siempre ordenada
    private final LinkedHashMap<Integer,GooseBox> boxes = new LinkedHashMap<>();

    public Board() {
        super();
        boxes.put(1,new GooseBox(118,875,GooseBoxType.INICIO));       //    1
        boxes.put(2,new GooseBox(240,872,GooseBoxType.NORMAL));       //    2
        boxes.put(3,new GooseBox(322,872,GooseBoxType.NORMAL));       //    3
        boxes.put(4,new GooseBox(400,872,GooseBoxType.NORMAL));       //    4
        boxes.put(5,new GooseBox(478,872,GooseBoxType.OCA));          //    5
        boxes.put(6,new GooseBox(563,872,GooseBoxType.PUENTE));       //    6
        boxes.put(7,new GooseBox(644,872,GooseBoxType.NORMAL));       //    7
        boxes.put(8,new GooseBox(718,856,GooseBoxType.NORMAL));       //    8
        boxes.put(9,new GooseBox(793,811,GooseBoxType.OCA));          //    9
        boxes.put(10,new GooseBox(851,751,GooseBoxType.NORMAL));      //    10
        boxes.put(11,new GooseBox(879,672,GooseBoxType.NORMAL));      //    11
        boxes.put(12,new GooseBox(885,597,GooseBoxType.PUENTE));      //    12
        boxes.put(13,new GooseBox(885,509,GooseBoxType.NORMAL));      //    13
        boxes.put(14,new GooseBox(885,433,GooseBoxType.OCA));         //    14
        boxes.put(15,new GooseBox(887,354,GooseBoxType.NORMAL));      //    15
        boxes.put(16,new GooseBox(885,276,GooseBoxType.NORMAL));      //    16
        boxes.put(17,new GooseBox(863,206,GooseBoxType.NORMAL));      //    17
        boxes.put(18,new GooseBox(817,143,GooseBoxType.OCA));         //    18
        boxes.put(19,new GooseBox(756,95,GooseBoxType.POSADA));       //    19
        boxes.put(20,new GooseBox(680,63,GooseBoxType.NORMAL));       //    20
        boxes.put(21,new GooseBox(598,63,GooseBoxType.NORMAL));       //    21
        boxes.put(22,new GooseBox(523,63,GooseBoxType.NORMAL));       //    22
        boxes.put(23,new GooseBox(439,63,GooseBoxType.OCA));          //    23
        boxes.put(24,new GooseBox(359,63,GooseBoxType.NORMAL));       //    24
        boxes.put(25,new GooseBox(290,63,GooseBoxType.NORMAL));       //    25
        boxes.put(26,new GooseBox(216,90,GooseBoxType.DADOS));        //    26
        boxes.put(27,new GooseBox(151,131,GooseBoxType.OCA));         //    27
        boxes.put(28,new GooseBox(93,200,GooseBoxType.NORMAL));       //    28
        boxes.put(29,new GooseBox(76,282,GooseBoxType.NORMAL));       //    30
        boxes.put(30,new GooseBox(76,369,GooseBoxType.NORMAL));       //    31
        boxes.put(31,new GooseBox(76,445,GooseBoxType.POZO));         //    32
        boxes.put(32,new GooseBox(76,531,GooseBoxType.OCA));          //    33
        boxes.put(33,new GooseBox(93,605,GooseBoxType.NORMAL));       //    34
        boxes.put(34,new GooseBox(127,675,GooseBoxType.NORMAL));      //    34
        boxes.put(35,new GooseBox(193,725,GooseBoxType.NORMAL));      //    35
        boxes.put(36,new GooseBox(275,756,GooseBoxType.OCA));         //    36
        boxes.put(37,new GooseBox(353,756,GooseBoxType.NORMAL));      //    37
        boxes.put(38,new GooseBox(436,756,GooseBoxType.NORMAL));      //    38
        boxes.put(39,new GooseBox(513,756,GooseBoxType.NORMAL));      //    39
        boxes.put(40,new GooseBox(598,756,GooseBoxType.NORMAL));      //    40
        boxes.put(41,new GooseBox(688,741,GooseBoxType.OCA));         //    41
        boxes.put(42,new GooseBox(751,693,GooseBoxType.LABERINTO));   //    42
        boxes.put(43,new GooseBox(763,605,GooseBoxType.NORMAL));      //    43
        boxes.put(44,new GooseBox(763,529,GooseBoxType.NORMAL));      //    44
        boxes.put(45,new GooseBox(763,448,GooseBoxType.OCA));         //    45
        boxes.put(46,new GooseBox(763,365,GooseBoxType.NORMAL));      //    46
        boxes.put(47,new GooseBox(763,288,GooseBoxType.NORMAL));      //    47
        boxes.put(48,new GooseBox(727,217,GooseBoxType.NORMAL));      //    48
        boxes.put(49,new GooseBox(652,185,GooseBoxType.NORMAL));      //    49
        boxes.put(50,new GooseBox(571,185,GooseBoxType.OCA));         //    50
        boxes.put(51,new GooseBox(492,185,GooseBoxType.NORMAL));      //    51
        boxes.put(52,new GooseBox(412,185,GooseBoxType.CARCEL));      //    52
        boxes.put(53,new GooseBox(335,185,GooseBoxType.DADOS));       //    53
        boxes.put(54,new GooseBox(260,204,GooseBoxType.OCA));         //    54
        boxes.put(55,new GooseBox(200,259,GooseBoxType.NORMAL));      //    55
        boxes.put(56,new GooseBox(197,350,GooseBoxType.NORMAL));      //    56
        boxes.put(57,new GooseBox(197,434,GooseBoxType.NORMAL));      //    57
        boxes.put(58,new GooseBox(197,510,GooseBoxType.CALAVERA));    //    58
        boxes.put(59,new GooseBox(219,598,GooseBoxType.OCA));         //    59
        boxes.put(60,new GooseBox(300,631,GooseBoxType.NORMAL));      //    60
        boxes.put(61,new GooseBox(387,631,GooseBoxType.NORMAL));      //    61
        boxes.put(62,new GooseBox(475,631,GooseBoxType.NORMAL));      //    62
        boxes.put(63,new GooseBox(594,543,GooseBoxType.FINAL));       //    63
        
    }

    public LinkedHashMap<Integer, GooseBox> getBoxes() {
        return boxes;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void addPlayer(Player player) {
        player.setIdPlayer(this.playerList.size());
        this.playerList.add(player);
    }

    public void movePlayer(Player player,int resultadoDato,Text boxDescription){
        int position = player.getCurrentPosition()+resultadoDato;
        this.getPlayerList()
                .get(player.getIdPlayer())
                .move(position,this.getBoxes().get(position),resultadoDato,boxDescription);
    }

}
