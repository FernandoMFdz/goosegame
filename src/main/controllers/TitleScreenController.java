package main.controllers;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.AudioClip;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import main.Main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TitleScreenController implements Initializable {
    public ImageView playButton;
    public ImageView quitButton;
    public CheckBox activateMusic;
    public Text mainText;

    private AudioClip au;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.au = new AudioClip(Main.class.getResource("/main/resources/sound/music/mix.mp3").toString());
        this.au.setVolume(0.25);
        this.au.play();
        ColorAdjust cA = new ColorAdjust();
        cA.setSaturation(-0.5);
        this.playButton.setEffect(cA);
        this.quitButton.setEffect(cA);
        Timeline timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame( Duration.seconds(2.5),new KeyValue(this.mainText.rotateProperty(), 4.4)));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.setAutoReverse(true);
        timeline.play();
    }

    // Este es el método mas importante de la pantalla de Inicio. Es el encargado de lanzar el juego
    public void startGame(MouseEvent mouseEvent) {
        Stage stage = (Stage) ((Node)mouseEvent.getSource()).getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main/fxml/gooseGame.fxml"));
        try {
            Parent root = loader.load();
            // Hago el cambio de escena
            stage.setScene(new Scene(root, 1255, 989));
            stage.setResizable(false);
            // Hago esto para inicializar el objeto AudioClip y que el GameScreenController sea capaz de pararlo o modificarlo.
            GameScreenController c = loader.getController();
            c.initAudioClip(this.au);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void musicChanged(MouseEvent inputMethodEvent) {
        if(activateMusic.isSelected()){
            this.au.play();
        } else {
            this.au.stop();
        }
    }

    public void overJugar(MouseEvent mouseEvent) {
        ColorAdjust cA = new ColorAdjust();
        cA.setSaturation(0);
        this.playButton.setEffect(cA);
    }

    public void exitedJugar(MouseEvent mouseEvent) {
        ColorAdjust cA = new ColorAdjust();
        cA.setSaturation(-0.5);
        this.playButton.setEffect(cA);
    }

    public void overQuit(MouseEvent mouseEvent) {
        ColorAdjust cA = new ColorAdjust();
        cA.setSaturation(0);
        this.quitButton.setEffect(cA);
    }

    public void exitedQuit(MouseEvent mouseEvent) {
        ColorAdjust cA = new ColorAdjust();
        cA.setSaturation(-0.5);
        this.quitButton.setEffect(cA);
    }

    public void quitGame(MouseEvent mouseEvent) {
        System.exit(0);
    }
}
