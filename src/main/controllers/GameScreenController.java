package main.controllers;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.AudioClip;
import javafx.scene.text.Text;
import javafx.util.Duration;
import main.Main;
import main.entities.Player;
import main.entities.Board;
import main.tasks.TimerTask;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicReference;

public class GameScreenController implements Initializable {

    public ImageView launchDice;
    private Timeline diceAnimationTimeline;


    //*     JUGADORES      *//
    public ImageView fichaPlayerOne;
    public ImageView fichaPlayerTwo;

    public Text textPlayerOnePosition;
    public Text textPlayerTwoPosition;

    public Text textPlayerOne;
    public Text textPlayerTwo;


    //*     CRONÓMETRO     *//
    public Text textTimerMinutes;
    public Text textTimerSeconds;
    public Text textTimerHours;
    public ImageView confettiWin;
    public ProgressBar gameProgress;

    private Board board;
    public Text textResultadoDado;

    private AudioClip au;
    public ImageView tableroImagen;
    public Text boxDescription;

    private TimerTask timerTask;
    private Player currentPlayer;

    private boolean userCanPushTheDice = true;

    public void initialize(URL url, ResourceBundle rb) {
        // Creo los jugadores
        Player playerOne = new Player(this.fichaPlayerOne, textPlayerOne, textPlayerOnePosition);
        Player playerTwo = new Player(this.fichaPlayerTwo, textPlayerTwo, textPlayerTwoPosition);
        this.board = new Board();
        this.board.addPlayer(playerOne);
        this.board.addPlayer(playerTwo);

        // Inicializo el temporizador
        startTimer();

        /*
             Aquí, en el inicio, hago una pequeña animación al entrar para guiar al jugador y que sepa donde tiene que pulsar. Simplemente hago que el dado
            crezca y decrezca para captrar la atención del jugador. Al pulsar el botón finalizo la animación. Con esto estoy incitando al usuario a pulsar donde
            quiero y que aprenda a jugar por sí mismo.
         */
        diceAnimationTimeline = new Timeline();
        diceAnimationTimeline.play();
        diceAnimationTimeline.getKeyFrames().add(new KeyFrame( Duration.seconds(0.7),new KeyValue(this.launchDice.fitHeightProperty(),this.launchDice.getFitHeight()+35)));
        diceAnimationTimeline.getKeyFrames().add(new KeyFrame( Duration.seconds(0.7),new KeyValue(this.launchDice.fitWidthProperty(),this.launchDice.getFitWidth()+35)));
        diceAnimationTimeline.setCycleCount(Animation.INDEFINITE);
        diceAnimationTimeline.setAutoReverse(true);
        diceAnimationTimeline.play();

    }

    private void startTimer(){
        this.timerTask = new TimerTask(textTimerMinutes,textTimerSeconds,textTimerHours);
        Thread timerThread = new Thread(this.timerTask);
        timerThread.setDaemon(false);
        timerThread.start();
    }

    protected void initAudioClip(AudioClip au){
        this.au = au;
    }

    private Player switchTurn() {
        // Hago esto para poder adelantarme y ver a quien le toca el siguiente turno pero sin hacer ningún cambio visual o al juego en si.
        // Simplemente con esto añado la posibilidad de saber quien va a ser el siguiente en jugar.
        return switchTurn(false);
    }

    private synchronized Player switchTurn(boolean affectUI){
        // Calculo el jugador del siguiente turno y hago los cambios necesarios.
        // Aquí se tiene en cuenta si el jugador se encuentra baneado o en una casilla tipo Oca

        Player originalPlayer = currentPlayer;
        Player finalPlayer = currentPlayer;

            if(finalPlayer == null){
                finalPlayer = this.board.getPlayerList().get(0); // El primer jugador somos nosotros
                originalPlayer = finalPlayer;

            } else {
                if( originalPlayer.turnsInGooseBox == 0){
                    AtomicReference<Player> finalPlayer1 = new AtomicReference<>(finalPlayer);
                    this.board.getPlayerList().stream()
                            .filter(player -> player != finalPlayer1.get())
                            .findFirst()
                            .ifPresent(finalPlayer1::set);
                    finalPlayer = finalPlayer1.get();
                    if(finalPlayer.isBanned()){
                        if(affectUI) finalPlayer.setTurnsBanned(finalPlayer.getTurnsBanned()-1);
                        finalPlayer = originalPlayer;
                    }
                } else {
                    if(affectUI) originalPlayer.turnsInGooseBox -= 2;
                }
            }
        if(affectUI){
            originalPlayer.scoreBoard.setStyle("-fx-font-size: 62;");
            originalPlayer.playerPosition.setStyle("-fx-font-size: 62;");

            finalPlayer.scoreBoard.setStyle("-fx-font-size: 72;");
            finalPlayer.playerPosition.setStyle("-fx-font-size: 72;");
            boxDescription.setFill(finalPlayer.scoreBoard.getFill());
        }
        return  finalPlayer;
    }

    public void lanzarDado(){
        lanzarDado(null);
    }

    public void lanzarDado(MouseEvent actionEvent) {
        // Paro la animación del dado. Una vez que el usuario sabe jugar no tiene sentido mantenerla.
        diceAnimationTimeline.stop();
        if(this.userCanPushTheDice || switchTurn().equals(this.board.getPlayerList().get(1))){
            Player turnPlayer = switchTurn(true);
            this.currentPlayer = turnPlayer;
            int resultadoDado = (int) (Math.random() * 10) + 1;
            textResultadoDado.setText(String.valueOf(resultadoDado));
            deactivateDice();

            // Muevo al jugador actual, sea quien sea.
            this.board.movePlayer(this.currentPlayer, resultadoDado,boxDescription);
            Timeline timeline = new Timeline();
            if( (this.currentPlayer.getCurrentPosition()) >= this.board.getBoxes().size()){
                // Si la posición del jugador es igual o superior a la del total de casillas entiendo que ha terminado el juego.
                timeline.getKeyFrames().add(new KeyFrame( Duration.seconds(1), new KeyValue(this.textResultadoDado.visibleProperty(), false)));
                timeline.setOnFinished(event -> {
                    deactivateDice();
                    this.au.stop();
                    this.timerTask.stop();
                    if(currentPlayer.getIdPlayer() == 0){
                        this.au = new AudioClip(Main.class.getResource("/main/resources/sound/win.mp3").toString());
                        confettiWin.setVisible(true);
                    } else {
                        this.au = new AudioClip(Main.class.getResource("/main/resources/sound/defeat.mp3").toString());
                        boxDescription.setText("Fin del juego. Has perdido :/");
                    }
                    au.setVolume(0.30);
                    au.play();
                });
            } else {
                // Si no ha terminado el juego sigo funcionando.
                timeline.getKeyFrames().add(new KeyFrame( Duration.seconds(1), new KeyValue(this.textResultadoDado.visibleProperty(), true)));
                timeline.setOnFinished((event) -> {
                    Player nextPlayer = switchTurn();
                    if(nextPlayer.equals(this.board.getPlayerList().get(1))){
                        lanzarDado();
                    } else {
                        activateDice();
                    }

                });
            }
            timeline.play();

            // Calculo el progreso del juego en base al estado actual máximo de los jugadores.
            int maxPosition = 0;
            for (Player player : this.board.getPlayerList()) {
                if(player.getCurrentPosition() > maxPosition){
                    maxPosition = player.getCurrentPosition();
                }
            }
            double progress = (maxPosition * 100)/this.board.getBoxes().size();
            gameProgress.setProgress(progress / 100);
        }
    }

    private void deactivateDice(){
        this.userCanPushTheDice = false;
        ColorAdjust colorAdjust = new ColorAdjust();
        colorAdjust.setSaturation(-1);
        colorAdjust.setContrast(-0.65);
        this.launchDice.setEffect(colorAdjust);
    }

    private void activateDice(){
        this.userCanPushTheDice = true;
        ColorAdjust colorAdjust = new ColorAdjust();
        colorAdjust.setSaturation(0);
        colorAdjust.setContrast(0);
        this.launchDice.setEffect(colorAdjust);
    }
}
