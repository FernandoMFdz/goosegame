package main.tasks;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class TimerTask extends Task {

    private Text textTimerMinutes;
    private Text textTimerSeconds;
    private Text textTimerHours;

    private Timeline hourTimeline;
    private Timeline minuteTimeline;
    private Timeline secondTimeline;

    private static final int STARTTIME = 0;

    private int timeSeconds = 0;
    private int timeMinutes = 0;
    private int timeHours = 0;

    private final StringProperty timeSecondsWrap = new SimpleStringProperty("00");
    private final StringProperty timeMinutesWrap = new SimpleStringProperty("00");
    private final StringProperty timeHoursWrap = new SimpleStringProperty("00");

    public TimerTask(Text textTimerMinutes, Text textTimerSeconds, Text textTimerHours) {
        this.textTimerMinutes = textTimerMinutes;
        this.textTimerSeconds = textTimerSeconds;
        this.textTimerHours = textTimerHours;
    }

    @Override
    protected Void call() throws Exception {
        // Inicializo los contadores y comienzo a refrescar los Text que vienen del FXML
        // Cada contador funciona de forma individual. Me costó un poco hacerlo funcionar
        // pero es bastante interesante para ver como funciona la concurrencia en
        // un entorno JavaFX
        this.hourTimeline = startTimeline(hourTimeline,Duration.hours(1), this::updateHours);
        this.minuteTimeline = startTimeline(minuteTimeline,Duration.minutes(1), this::updateMinutes);
        this.secondTimeline = startTimeline(secondTimeline,Duration.seconds(1), this::updateSeconds);

        return null;
    }

    public void stop(){
        this.hourTimeline.stop();
        this.minuteTimeline.stop();
        this.secondTimeline.stop();

        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0.35), evt -> {
            textTimerMinutes.setVisible(false);
            textTimerHours.setVisible(false);
            textTimerSeconds.setVisible(false);
        }),
                new KeyFrame(Duration.seconds( 0.7), evt -> {
                    textTimerMinutes.setVisible(true);
                    textTimerHours.setVisible(true);
                    textTimerSeconds.setVisible(true);
                }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();

    }

    private Timeline startTimeline(Timeline tl,Duration d, Runnable runnable){
        tl = new Timeline();
        tl.getKeyFrames().add(new KeyFrame(d, evnt -> runnable.run()));
        tl.setCycleCount(Animation.INDEFINITE);
        tl.play();

        return tl;
    }

    private void updateSeconds() {
        timeSeconds += 1;
        if(timeSeconds == 60) timeSeconds = 0;

        if(timeSeconds < 10 ){
            timeSecondsWrap.setValue("0"+timeSeconds);
        } else timeSecondsWrap.setValue(String.valueOf(timeSeconds));

        textTimerSeconds.textProperty().bind(timeSecondsWrap);
    }

    private void updateMinutes() {
        timeMinutes += 1;
        if(timeMinutes == 60) timeMinutes = 0;

        if(timeMinutes < 10 ){
            timeMinutesWrap.setValue("0"+timeMinutes);
        } else timeMinutesWrap.setValue(String.valueOf(timeMinutes));

        textTimerMinutes.textProperty().bind(timeMinutesWrap);
    }

    private void updateHours() {
        timeHours += 1;
        if(timeHours < 10 ){
            timeHoursWrap.setValue("0"+timeHours);
        } else timeHoursWrap.setValue(String.valueOf(timeHours));

        textTimerHours.textProperty().bind(timeHoursWrap);
    }




}
