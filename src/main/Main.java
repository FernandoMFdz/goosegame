package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        // Cargo la fuente personalizada.
        Font.loadFont(getClass().getResourceAsStream("/main/resources/fonts/game_over.ttf"), 200);

        // Cargo la pantalla de inicio en primera instancia.
        Parent root = FXMLLoader.load(getClass().getResource("fxml/titleScreen.fxml"));
        primaryStage.setTitle("The Goose Game"); // Añado el nombre de la ventana
        primaryStage.getIcons().add(new Image("/main/resources/images/icon.png")); // Añado un icono. Puramente visual.
        primaryStage.setScene(new Scene(root, 1255, 989));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
